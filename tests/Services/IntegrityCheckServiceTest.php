<?php

namespace App\Tests\Services;

use App\Util\IntegrityCheckService;
use PHPUnit\Framework\TestCase;

/**
 * Class IntegrityCheckServiceTest
 * @package App\Tests\Services
 */
class IntegrityCheckServiceTest extends TestCase
{
    /**
     * Test integrity-check  functionality (success scenario)
     */
    public function testIntegrityCheckSuccess(): void
    {
        $integrityCheckService = new IntegrityCheckService();
        $dataArr = [
            'firstName' => 'Max',
            'lastName' => 'Mustermann',
            'email' => 'test@example.com',
        ];

        $status = $integrityCheckService->integrityCheck($dataArr);
        $this->assertTrue($status);
    }

    /**
     * Test integrity-check functionality (Failure scenario)
     */
    public function testIntegrityCheckFails(): void
    {
        $integrityCheckService = new IntegrityCheckService();
        $dataArr = [
            'firstName' => 'Max',
            'lastName' => 'Mustermann',
        ];

        $status = $integrityCheckService->integrityCheck($dataArr);
        $this->assertFalse($status);
    }

    //TODO: all CRUD functions in ApiController to tested later
}