<?php

namespace App\Util;

/**
 * Class IntegrityCheckService
 * @package App\Util
 */
class IntegrityCheckService
{

    /**
     * @var array
     */
    private static $customerRequiredFields = ['firstName', 'lastName', 'email'];

    /**
     * @param $givenArray
     *
     * @return bool
     */
    public function integrityCheck($givenArray): bool
    {
        foreach (self::$customerRequiredFields as $value)
        {
            if(!array_key_exists($value, $givenArray)){
                return false;
            }
        }

        return true;
    }

}