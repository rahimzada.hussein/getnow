<?php

namespace App\Controller;

use App\Repository\CustomersRepository;
use App\Repository\OrdersRepository;
use App\Util\IntegrityCheckService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractController
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ApiController constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Fetch info of all customers
     *
     * @Route("/api/allcustomers", name="api_all_customers",
     *     methods={"GET"},
     *     defaults={"_format":"json"}
     * )
     *
     * @param CustomersRepository $customersRepository
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function fetchAllCustomers(CustomersRepository $customersRepository, TranslatorInterface $translator): JsonResponse
    {
        try {
            $status = Response::HTTP_OK;
            $data = $customersRepository->findAll();
            $resultArr = [];
            if ($data) {
                foreach ($data as $key => $value) {
                    $resultArr[$key]['id'] = $value->getId();
                    $resultArr[$key]['firstName'] = $value->getFirstName();
                    $resultArr[$key]['lastName'] = $value->getLastName();
                    $resultArr[$key]['email'] = $value->getEmail();
                    $resultArr[$key]['order'] = '';
                    //fetch order collection
                    if ($order = $value->getOrders()->current()) {
                        $resultArr[$key]['order'] = [
                            'orderStatus' => $order->getOderStatus(),
                            'orderQuantity' => $order->getQuantity(),
                            'orderDate' => $order->getOrderDate(),
                        ];
                    }
                    $resultArr[$key]['createdAt'] = $value->getCreatedAt();
                }
            } else {
                $resultArr = ['error' => $translator->trans('no_customer_found')];
            }

        } catch (\Exception $e) {
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Fetch customer info for given customer ID
     *
     * @Route("/api/customer/{id}", name="api_customer_info",
     *      requirements={"id": "\d+"},
     *      methods={"GET"},
     *      defaults={"_format":"json"}
     * )
     *
     * @param integer $id
     * @param CustomersRepository $customersRepository
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function fetchCustomerInfo($id, CustomersRepository $customersRepository, TranslatorInterface $translator): JsonResponse
    {
        try {
            $status = Response::HTTP_OK;
            $data = $customersRepository->findOneBy(['id' => $id]);
            if ($data) {
                $resultArr = [];
                $resultArr['id'] = $data->getId();
                $resultArr['firstName'] = $data->getFirstName();
                $resultArr['lastName'] = $data->getLastName();
                $resultArr['email'] = $data->getEmail();
                $resultArr['order'] = '';
                //fetch order collection
                if ($order = $data->getOrders()->current()) {
                    $resultArr['order'] = [
                        'orderStatus' => $order->getOderStatus(),
                        'orderQuantity' => $order->getQuantity(),
                        'orderDate' => $order->getOrderDate(),
                    ];
                }
                $resultArr['createdAt'] = $data->getCreatedAt();
            } else {
                $status = Response::HTTP_BAD_REQUEST;
                $resultArr = ['error' => $translator->trans('no_customer_found_for_id') . $id];
            }

        } catch (\Exception $e) {
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Insert customer
     *
     * @Route("/api/customer/insert", name="api_customer_insert",
     *      methods={"POST"},
     *      defaults={"_format":"json"}
     * )
     *
     * @param CustomersRepository $customersRepository
     * @param IntegrityCheckService $integrityCheckService
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     */
    public function insertCustomer(CustomersRepository $customersRepository, IntegrityCheckService $integrityCheckService, EntityManagerInterface $em, TranslatorInterface $translator): JsonResponse
    {
        $status = Response::HTTP_CREATED;
        try {
            $customerData = $_POST['customerData'];
            $customerDataDecode = json_decode($customerData);
            if ($integrityCheckService->integrityCheck($customerDataDecode)) {
                if (filter_var($customerDataDecode->email, FILTER_VALIDATE_EMAIL)) {
                    $resultArr = $customersRepository->insertCustomerViaApi($customerDataDecode, $em);

                    if (is_array($resultArr)) {
                        $resultArr = ['success' => sprintf('A new customer with id (%d) and full name (%s) has been created', $resultArr['id'], $resultArr['fullName'])];
                    } else {
                        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                        $resultArr = ['error' => $resultArr];
                    }
                } else {
                    $status = Response::HTTP_BAD_REQUEST;
                    $resultArr = ['error' => $translator->trans('invalid_email')];
                }

            } else {
                $status = Response::HTTP_BAD_REQUEST;
                $resultArr = ['error' => $translator->trans('all_required_fields')];
            }

        } catch (\Exception $e) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Delete customer for given customer ID
     *
     * @Route("/api/cutomer/delete/{id}", name="api_customer_delete",
     *      requirements={"id": "\d+"},
     *      methods={"DELETE"},
     *      defaults={"_format":"json"}
     * )
     *
     * @param integer $id
     * @param CustomersRepository $customersRepository
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     */
    public function deleteCustomer($id, CustomersRepository $customersRepository, EntityManagerInterface $em, TranslatorInterface $translator): JsonResponse
    {
        try {
            //it would be better to use response code 204, but for sake of presentation the status code 200 is used
            //$status = Response::HTTP_NO_CONTENT;
            $status = Response::HTTP_OK;
            $resultArr =  ['success' => $translator->trans('customer_success_delete')];
            $customer = $customersRepository->find($id);
            if ($customer) {
                $em->remove($customer);
                $em->flush();
            } else {
                $status = Response::HTTP_BAD_REQUEST;
                $resultArr = ['error' => $translator->trans('no_customer_found_for_id') . $id];
            }

        } catch (\Exception $e) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Update customer for given customer ID
     *
     * @Route("/api/customer/update", name="api_customer_update",
     *      methods={"PUT"},
     *      defaults={"_format":"json"}
     * )
     *
     * @param Request $request
     * @param CustomersRepository $customersRepository
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     */
    public function updateCustomer(Request $request, CustomersRepository $customersRepository, EntityManagerInterface $em, TranslatorInterface $translator): JsonResponse
    {
        try {
            $status = Response::HTTP_OK;
            $customerData = $request->headers->get('customerData');
            $customerDataDecode = json_decode($customerData);
            if ($customerDataDecode && $email = $customerDataDecode->email) {
                $customer = $customersRepository->findOneBy(['email' => $email]);

                if ($customer) {
                    $resultArr = $customersRepository->updateCustomerViaApi($customer, $customerDataDecode, $em);

                    if (is_array($resultArr)) {
                        $resultArr = ['success' => sprintf('%s with id (%d) has been updated', $resultArr['fullName'], $resultArr['id'])];
                    } else {
                        $resultArr = ['error' => $resultArr];
                    }
                } else {
                    $status = Response::HTTP_BAD_REQUEST;
                    $resultArr = ['error' => $translator->trans('no_customer_found_for_email') . $email];
                }
            } else {
                $status = Response::HTTP_BAD_REQUEST;
                $resultArr = ['error' => $translator->trans('no_customer_found_for_email') . $email];
            }

        } catch (\Exception $e) {
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Fetch info of all orders
     *
     * @Route("/api/allorders", name="api_all_orders",
     *     methods={"GET"},
     *     defaults={"_format":"json"}
     * )
     *
     * @param OrdersRepository $ordersRepository
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function fetchAllOrders(OrdersRepository $ordersRepository, TranslatorInterface $translator): JsonResponse
    {
        try {
            $status = Response::HTTP_OK;
            $data = $ordersRepository->findAll();
            $resultArr = [];
            if ($data) {
                foreach ($data as $key => $value) {
                    $resultArr[$key]['id'] = $value->getId();
                    $resultArr[$key]['quantity'] = $value->getQuantity();
                    $resultArr[$key]['lastName'] = $value->getOderStatus();
                    $resultArr[$key]['customer'] = '';
                    //fetch customer
                    if ($customer = $value->getCustomer()) {
                        $resultArr[$key]['customer'] = [
                            'customerId' => $customer->getId(),
                            'customerFirstName' => $customer->getFirstName(),
                            'customerLastName' => $customer->getLastName(),
                            'customerEmail' => $customer->getEmail(),
                        ];
                    }
                    $resultArr[$key]['orderDate'] = $value->getOrderDate();
                }
            } else {
                $resultArr = ['error' => $translator->trans('no_order_found')];
            }

        } catch (\Exception $e) {
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($resultArr, $status);
    }

    /**
     * Fetch order info for given order ID
     *
     * @Route("/api/order/{id}", name="api_order_info",
     *      requirements={"id": "\d+"},
     *      methods={"GET"},
     *      defaults={"_format":"json"}
     * )
     *
     * @param integer $id
     * @param OrdersRepository $ordersRepository
     * @param TranslatorInterface $translator
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function fetchOrderInfo($id, OrdersRepository $ordersRepository, TranslatorInterface $translator): JsonResponse
    {
        try {
            $status = Response::HTTP_OK;
            $data = $ordersRepository->findOneBy(['id' => $id]);
            if ($data) {
                $resultArr = [];
                $resultArr['id'] = $data->getId();
                $resultArr['firstName'] = $data->getQuantity();
                $resultArr['lastName'] = $data->getOderStatus();
                $resultArr['customer'] = '';
                //fetch customer
                if ($customer = $data->getCustomer()) {
                    $resultArr['customer'] = [
                        'customerId' => $customer->getId(),
                        'customerFirstName' => $customer->getFirstName(),
                        'customerLastName' => $customer->getLastName(),
                        'customerEmail' => $customer->getEmail(),
                    ];
                }
                $resultArr['createdAt'] = $data->getOrderDate();
            } else {
                $status = Response::HTTP_BAD_REQUEST;
                $resultArr = ['error' => $translator->trans('no_order_found_for_id') . $id];
            }

        } catch (\Exception $e) {
            $resultArr = ['error' => $e->getMessage()];
            $this->logger->error($e->getMessage());
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($resultArr, $status);
    }
}
