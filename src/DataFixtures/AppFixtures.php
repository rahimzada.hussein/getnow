<?php

namespace App\DataFixtures;

use App\Entity\Customers;
use App\Entity\Orders;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {

        //PHP array containing firstName
        $firstName = array(
            'Christopher',
            'Ryan',
            'Ethan',
            'John',
            'Zoey',
            'Sarah',
            'Michelle',
            'Samantha',
        );

        //PHP array containing lastName
        $lastName = array(
            'Walker',
            'Thompson',
            'Anderson',
            'Johnson',
            'Tremblay',
            'Peltier',
            'Cunningham',
            'Simpson',
            'Mercado',
            'Sellers'
        );

        $orderStatus = array(
            'ordered',
            'shipped',
            'packed',
            'returned',
            'cancelled',
            'delivered',
        );

        for ($i = 0; $i < 10; $i++) {
            $order = new Orders();
            $customer = new Customers();

            $customer->setFirstName($firstName[random_int(0, count($firstName) - 1)]);
            $customer->setLastName($lastName[random_int(0, count($lastName) - 1)]);
            $customer->setEmail(sprintf('test%d@example.com', $i));
            $manager->persist($customer);

            $order->setCustomer($customer);
            $order->setQuantity(random_int(0, 10));
            $order->setOderStatus($orderStatus[random_int(0, count($orderStatus) - 1)]);
            $manager->persist($order);
            $manager->flush();
        }

    }
}
