<?php

namespace App\Repository;

use App\Entity\Customers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Customers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customers[]    findAll()
 * @method Customers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Customers::class);
    }

    /**
     * @param $customerData
     * @param $em
     *
     * @return mixed
     */
    public function insertCustomerViaApi($customerData, $em)
    {
        try {
            $customer = new Customers();
            $customer->setFirstName($customerData->firstName)
                ->setLastName($customerData->lastName)
                ->setEmail($customerData->email);

            $em->persist($customer);
            $em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }
        return ['id' => $customer->getId(), 'fullName' => $customerData->firstName . ' ' . $customerData->lastName];
    }

    /**
     * @param Customers $customer
     * @param array $customerData
     * @param EntityManagerInterface $em
     *
     * @return mixed
     */
    public function updateCustomerViaApi($customer, $customerData, $em)
    {
        try {
            foreach ($customerData as $key => $value) {
                if ($key === 'firstName') {
                    $customer->setFirstName($value);
                } elseif ($key === 'lastName') {
                    $customer->setLastName($value);
                } elseif ($key === 'email') {
                    $customer->setEmail($value);
                }
            }

            $em->flush();

        } catch (\Exception $e) {

            return $e->getMessage();
        }

        return ['id' => $customer->getId(), 'fullName' => $customer->getFirstName() . ' ' . $customer->getLastName()];
    }
}
